﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Minimalistic.Wpf.Toolkit.Behaviours
{
    /// <summary>
    /// From a blog post of Oliver Trenouth WPF: Drag Parent Window Behaviour
    /// (https://olitee.com/2015/04/wpf-drag-parent-window-behaviour/)
    /// </summary>
    public class DragWindowBehaviour : Behavior<FrameworkElement>
    {
        private Window _parentWindow;

        protected override void OnAttached()
        {
            _parentWindow = GetParentWindow(AssociatedObject);
            if (_parentWindow == null)
            {
                return;
            }

            AssociatedObject.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _parentWindow.DragMove();
        }

        private static Window GetParentWindow(DependencyObject attachedElement)
        {
            return Window.GetWindow(attachedElement);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseLeftButtonDown -= OnMouseLeftButtonDown;
            _parentWindow = null;
        }
    }
}
