﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Minimalistic.Wpf.Toolkit.Behaviours
{
    /// <summary>
    /// From Nish Nishant (https://www.codeproject.com/Members/Nish-Nishant) in the article
    /// (https://www.codeproject.com/Articles/73251/Handling-a-Window-s-Closed-and-Closing-events-in-t).
    /// Licensed under the Code Project Open License (CPOL) 1.02
    /// (https://www.codeproject.com/info/cpol10.aspx)
    /// Mentioned on stackoverflow
    /// (https://stackoverflow.com/questions/36129372/how-to-cancel-window-closing-in-mvvm-wpf-application).
    /// </summary>
    public class WindowClosingBehavior
    {
        public static ICommand GetClosed(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(ClosedProperty);
        }

        public static void SetClosed(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ClosedProperty, value);
        }

        public static readonly DependencyProperty ClosedProperty =
            DependencyProperty.RegisterAttached(
                "Closed", typeof(ICommand), typeof(WindowClosingBehavior),
                new UIPropertyMetadata(new PropertyChangedCallback(ClosedChanged)));

        private static void ClosedChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            if (target is Window window)
            {
                if (e.NewValue != null)
                {
                    window.Closed += OnWindowClosed;
                }
                else
                {
                    window.Closed -= OnWindowClosed;
                }
            }
        }

        private static void OnWindowClosed(object sender, EventArgs e)
        {
            ICommand closed = GetClosed(sender as Window);

            if (closed != null)
            {
                closed.Execute(null);
            }
        }

        public static ICommand GetClosing(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(ClosingProperty);
        }

        public static void SetClosing(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ClosingProperty, value);
        }

        public static readonly DependencyProperty ClosingProperty =
            DependencyProperty.RegisterAttached(
                "Closing", typeof(ICommand), typeof(WindowClosingBehavior),
                new UIPropertyMetadata(new PropertyChangedCallback(ClosingChanged)));

        private static void ClosingChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            if (target is Window window)
            {
                if (e.NewValue != null)
                {
                    window.Closing += OnWindowClosing;
                }
                else
                {
                    window.Closing -= OnWindowClosing;
                }
            }
        }

        private static void OnWindowClosing(object sender, CancelEventArgs e)
        {
            ICommand closing = GetClosing(sender as Window);

            if (closing != null)
            {
                if (closing.CanExecute(null))
                {
                    closing.Execute(null);
                }
                else
                {
                    ICommand cancelClosing = GetCancelClosing(sender as Window);

                    if (cancelClosing != null)
                    {
                        cancelClosing.Execute(null);
                    }

                    e.Cancel = true;
                }
            }
        }

        public static ICommand GetCancelClosing(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CancelClosingProperty);
        }

        public static void SetCancelClosing(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CancelClosingProperty, value);
        }

        public static readonly DependencyProperty CancelClosingProperty =
            DependencyProperty.RegisterAttached(
                "CancelClosing", typeof(ICommand), typeof(WindowClosingBehavior));
    }
}
