﻿using System;

namespace Minimalistic.Wpf.Toolkit.ViewModel
{
    public class DefaultViewModelFactory : IViewModelFactory
    {
        public ViewModelBase Create(Type viewModelType)
        {
            return Activator.CreateInstance(viewModelType) as ViewModelBase;
        }
    }
}
