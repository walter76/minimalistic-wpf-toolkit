﻿using System;

namespace Minimalistic.Wpf.Toolkit.ViewModel
{
    public interface IViewModelFactory
    {
        ViewModelBase Create(Type viewModelType);
    }
}
