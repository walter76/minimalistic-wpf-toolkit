# Minimalistic.Wpf.Toolkit #

The *Minimalistic.Wpf.Toolkit* is a WPF Toolkit library I have developed for use in my private
projects. I love building applications according to the
[MVVM design pattern ](https://docs.microsoft.com/de-de/xamarin/xamarin-forms/enterprise-application-patterns/mvvm)
and all the code that is re-used throughout my projects is put here.

Feel free to use it at your own risk!
